import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import Search from 'grommet/components/Search';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Meter from 'grommet/components/Meter';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';
import Menu from 'grommet/components/Menu';
import Actions from 'grommet/components/icons/base/Actions';
import MenuIcon from 'grommet/components/icons/base/Menu';
import MailIcon from 'grommet/components/icons/base/Mail';
import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl';
import Layer from 'grommet/components/Layer';
import Button from 'grommet/components/Button';
import NavControl from '../components/NavControl';
import Responsive from 'grommet/utils/Responsive';

import NavMenu from '../components/NavControl';
import Slider from '../components/Slider';
import Features from '../components/Features';
import PopularPackage from '../components/PopularPackage';
import WhySunHealth from '../components/WhySunHealth';
import FAQ from '../components/FAQ';
import Steps from '../components/Steps';
import Footer from '../components/Footer';
import SearchBar from '../components/SearchBar';
import { pageLoaded } from './utils';

import { loadPackages } from '../actions/popularPackage';
import { loadFaqs } from '../actions/faq';

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    //pageLoaded('Tasks');
    //loadPackages();
    this.props.dispatch(loadPackages());
    this.props.dispatch(loadFaqs());
  }

  render() {
    return (
      <Article primary={true}>
        <NavMenu />
        {/* <SearchBar /> */}
        <Slider />
        <Features />
        <PopularPackage packages={this.props.packages} />
        <WhySunHealth />
        <Steps />
        {/* <FAQ faqs={this.props.faqs} /> */}
        <Footer />
      </Article>
    );
  }
}

Dashboard.defaultProps = {
  error: undefined,
  packages: [],
  faqs: [],
};

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
  packages: PropTypes.arrayOf(PropTypes.object),
  faqs: PropTypes.arrayOf(PropTypes.object),
};

Dashboard.contextTypes = {
  intl: PropTypes.object,
};

const select = state => ({ ...state.packages, ...state.faq });

export default connect(select)(Dashboard);
