import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Article from 'grommet/components/Article';
import Footers from '../components/Footer';
import NavControl from '../components/NavControl';
import SearchBar from '../components/SearchBar';
import { loadSearch } from '../actions/search';

//import Form from 'grommet/components/Form';

class Package extends Component {
  constructor() {
    super();
    this.state = {
      shedule: false,
    };
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);
  }
  _onClick() {
    this.setState({ shedule: true });
  }
  _onLayerClose() {
    this.setState({ shedule: false });
  }

  render() {
    console.log('confidanceeeeeeeeeee',this.props);
    return (
      <Article>
        <NavControl />
       <Box>
       <SearchBar />
        </Box>

        <Footers />
      </Article>
    );
  }
}



// Package.defaultProps = {
//   error: undefined,
//   user: {},
// };

// Package.propTypes = {
//   dispatch: PropTypes.func.isRequired,
//   error: PropTypes.object,
//   tasks: PropTypes.arrayOf(PropTypes.object),
// };

// Package.contextTypes = {
//   intl: PropTypes.object,
// };

const select = state => ({ ...state });

export default connect(select)(Package);

