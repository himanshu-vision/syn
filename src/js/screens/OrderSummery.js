import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import FormFields from 'grommet/components/FormFields';
import NumberInput from 'grommet/components/NumberInput';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';
import Add from 'grommet/components/icons/base/Add';
import FormPrevious from 'grommet/components/icons/base/FormPrevious';
import Close from 'grommet/components/icons/base/Close';
import CheckBox from 'grommet/components/CheckBox';
import LoginForm from 'grommet/components/LoginForm';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import Article from 'grommet/components/Article';
import MailIcon from 'grommet/components/icons/base/Mail';
import Calendar from 'grommet/components/icons/base/Calendar';
import Clock from 'grommet/components/icons/base/Clock';
import MrpSummery from '../components/MrpSummery';
import OrderSummery from '../components/OrderSummery';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Logo from 'grommet/components/icons/Grommet';
import Footers from '../components/Footer';
import NavControl from '../components/NavControl';
import Tracking from '../components/Tracking';
import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl';
import MrpPaymentSummery from '../components/MrpPaymentSummery';

//import Form from 'grommet/components/Form';

class Cart extends Component {
  constructor() {
    super();
    this.state = {
      shedule: false,
    };
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);
  }
  _onClick() {
    this.setState({ shedule: true });
  }
  _onLayerClose() {
    this.setState({ shedule: false });
  }

  render() {
    return (
      <Article>
        <NavControl />
        <Box align="center" justify="center" margin={{ vertical: 'medium' }}>
          <Heading strong={true} truncate={true} tag="h1">
            Order Summery
          </Heading>
        </Box>
        <Box
          direction={'row'}
          justify="center"
          align="center"
          wrap={true}
          pad="medium"
          margin="small"
          colorIndex="light-1"
        >
          <Heading strong={true} truncate={true} tag="h2">
            Your booking Id is 1223211
          </Heading>
          <Button
            icon={<AccessVolumeControl />}
            //onClick={...}
            label="9784359717"
            plain={true}
            href="#"
          />
          <Button
            icon={<MailIcon />}
            label="hari@mohan.com"
            plain={true}
            //onClick={...}
            href="#"
          />
          <Button
            icon={<Calendar />}
            label="hari@mohan.com"
            plain={true}
            //onClick={...}
            href="#"
          />
          <Button
            icon={<Clock />}
            label="hari@mohan.com"
            plain={true}
            //onClick={...}
            href="#"
          />
        </Box>

        <Box
          direction={'row'}
          justify="start"
          align="start"
          wrap={true}
          full={'horizontal'}
          colorIndex="light-1"
        >
          <Header
            fixed={false}
            colorIndex={'neutral-1-a'}
            full={'horizontal'}
            float={false}
          >
            <Box full={'horizontal'} pad={{ horizontal: 'large' }}>
              <Title>Patient Name : Ankit Khandelwal, 25, male</Title>
            </Box>
          </Header>
          <Box direction={'row'}>
            <Box>
              <OrderSummery />
            </Box>
            <Box pad={'medium'}>
              <MrpSummery />
            </Box>
          </Box>
        </Box>

        <Box
          direction={'row'}
          justify="center"
          align="center"
          wrap={true}
          full={'horizontal'}
          colorIndex="light-1"
        >
          <Header
            fixed={false}
            colorIndex={'neutral-1-a'}
            full={'horizontal'}
            float={false}
          >
            <Box
              full={'horizontal'}
              justify="center"
              align="center"
              pad={{ horizontal: 'large' }}
            >
              <Title>Payment Detail</Title>
            </Box>
          </Header>
          <Box direction={'row'}>
            <Box pad={'medium'}>
              <MrpPaymentSummery />
            </Box>
          </Box>
        </Box>


        <Box
          direction={'row'}
          justify="center"
          align="center"
          wrap={true}
          full={'horizontal'}
          colorIndex="light-1"
        >
          <Header
            fixed={false}
            colorIndex={'neutral-1-a'}
            full={'horizontal'}
            float={false}
          >
            <Box
              full={'horizontal'}
              justify="center"
              align="center"
              pad={{ horizontal: 'large' }}
            >
              <Title> Oreder Tracking </Title>
            </Box>
          </Header>
          <Box direction={'row'}>
            <Box pad={'medium'}>
              <Tracking />
            </Box>
          </Box>
        </Box>

        <Footers />
      </Article>
    );
  }
}

export default Cart;
