import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import Headline from 'grommet/components/Headline';

import Article from 'grommet/components/Article';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Image from 'grommet/components/Image';
import Button from 'grommet/components/Button';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';
import Footer from '../components/Footer';
import NavControl from '../components/NavControl';


class NotFound extends Component {
  componentDidMount() {
    pageLoaded('Not Found');
    this.props.dispatch(navEnable(false));
  }

  componentWillUnmount() {
    this.props.dispatch(navEnable(true));
  }

  render() {
    return (
      <Article primary={true}>
      <NavControl />
      <Box
        full={true}
        direction={'row'}
        responsive={true}
        align="center"
        justify="center"
      >
      
        <Box pad={'small'} align={'center'}>
          <Headline strong={true}>404</Headline>
          <Heading>Oops...</Heading>
          <Paragraph size="large" align="center">
            It seems that you are in the wrong route. Please check your URL and
            try again.
          </Paragraph>
          <Button
            label="Go Home Page"
            //onClick={...}
            href="#"
          />
        </Box>
        <Box>
          <Image src="../img/404.gif" fit="contain" />
        </Box>
       
      </Box>
      <Footer />
      </Article>
    );
  }
}

NotFound.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(NotFound);
