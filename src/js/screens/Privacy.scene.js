import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import Search from 'grommet/components/Search';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Meter from 'grommet/components/Meter';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';
import Menu from 'grommet/components/Menu';
import Actions from 'grommet/components/icons/base/Actions';
import MenuIcon from 'grommet/components/icons/base/Menu';
import MailIcon from 'grommet/components/icons/base/Mail';
import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl';
import Layer from 'grommet/components/Layer';
import Button from 'grommet/components/Button';
import NavControl from '../components/NavControl';
import Responsive from 'grommet/utils/Responsive';

import NavMenu from '../components/NavControl';
import Slider from '../components/Slider';
import Features from '../components/Features';
import PopularPackage from '../components/PopularPackage';
import WhySunHealth from '../components/WhySunHealth';
import FAQ from '../components/FAQ';
import Steps from '../components/Steps';
import Footer from '../components/Footer';
import SearchBar from '../components/SearchBar';
import { pageLoaded } from './utils';

import { loadPackages } from '../actions/popularPackage';
import { loadFaqs } from '../actions/faq';

class PrivacyScene extends Component {
  render() {
    return (
      <Article primary={true}>
        <NavMenu />
        <div style={{ marginLeft: 50, marginRight: 50 }}>
          <Title style={{ margin: 100, marginLeft: 0 }}>
            Privacy & Refund policies
          </Title>
          Just like we make it so simple you to book an order for a desired
          health care package, we also make it hassle free to get refunds, if
          things did not work as planned. And to be easy to explain, here are
          some circumstances under which you could get you refunds. If samples
          not collected: When the amount paid will be refunded in scenarios like
          booking has been done and there is no service provider in that area or
          if you happens to booked multiple bookings due to system error or even
          if service-provider has not made any adequate attempt to provide
          service. In all the other scenarios other than ones mentioned
          above, 0-3% will be deducted as cancellation charges. If collected but
          left unprocessed: in such cases the refund after debiting the handling
          charges of Rs. 200 to Rs. 300 per order shall be processed as per
          taxation. In case laboratory fails: Though as per the records suggests
          there are slight chances that due to any technical complication and in
          such cases the service provider shall be advised to collect the fresh
          specimen and thoroughly process it free of cost. And if you also
          reserve the right to ask for refund which shall be granted in full to
          him/her as the case may be. The specimen re-collection has to be done
          within 15 days, post which no retesting is possible.
        </div>
        <Footer />
      </Article>
    );
  }
}

export default PrivacyScene;
