import { CART_LOAD, CART_UNLOAD } from '../actions';
import { fetchCart } from '../api/cart';

export function loadCart() {
  return dispatch =>
  fetchCart()
      .then(payload => {
        dispatch({ type: CART_LOAD, payload: payload.response });
      })
      .catch(payload => {
      });
}
