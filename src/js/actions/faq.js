import { FAQS_LOAD, FAQS_UNLOAD, FAQ_LOAD, FAQ_UNLOAD } from '../actions';
import {
  fetchFaq, 
} from '../api/faq';

export function loadFaqs() {
  
  return dispatch => (
    fetchFaq().then((payload)=>{
    let payloads = JSON.parse(payload);
    dispatch({ type: FAQS_LOAD, payload: payloads.response })
    console.log('payloads.response', payloads.response)
    
  }).catch(payload => {
  })
);
  //return dispatch => (
  //   fetchPopular()
  //     .on('success',
  //       payload => dispatch({ type: TASKS_LOAD, payload })
  //     )
  //     .on('error',
  //       payload => dispatch({ type: TASKS_LOAD, error: true, payload })
  //     )
  //     .start()
  // );
}

// export function unloadPackages() {
//   unwatchTasks();
//   return { type: TASKS_UNLOAD };
// }

