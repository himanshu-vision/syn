import { USER_LOAD, USER_UNLOAD } from '../actions';
import { fetchUser } from '../api/user';

export function loadUser() {
  return dispatch =>
    fetchUser()
      .then(payload => {
        dispatch({ type: USER_LOAD, payload: payload.response });
      })
      .catch(payload => {
        // alert('something error reLoad page');
      });
}
