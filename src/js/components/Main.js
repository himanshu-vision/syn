import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import App from 'grommet/components/App';
import Split from 'grommet/components/Split';

import NavSidebar from './NavSidebar';
import { navResponsive } from '../actions/nav';

import Login from '../screens/Login';
import Dashboard from '../screens/Dashboard';
import Home from '../screens/Home';
import Tasks from '../screens/Tasks';
import Task from '../screens/Task';
import NotFound from '../screens/NotFound';
import ContactUs from '../screens/ContactUs';
import Feedback from '../screens/Feedback';
import Prescription from '../screens/Prescription';
import List from '../screens/List';
import Cart from '../screens/cart';
import CheckOut from '../screens/CheckOut';
import OrderSummery from '../screens/OrderSummery';
import Package from '../screens/Package';
import TermsAndConditionsScene from '../screens/TermsAndConditions.scene';
import PrivacyScene from '../screens/Privacy.scene';

class Main extends Component {
  constructor() {
    super();
    this._onResponsive = this._onResponsive.bind(this);
  }

  _onResponsive(responsive) {
    this.props.dispatch(navResponsive(responsive));
  }

  render() {
    const {
      nav: { active: navActive, enabled: navEnabled, responsive },
    } = this.props;
    const includeNav = navActive && navEnabled;
    let nav;
    if (includeNav) {
      nav = <NavSidebar />;
    }
    const priority = includeNav && responsive === 'single' ? 'left' : 'right';

    return (
      <App centered={false}>
        <Router>
          <div>
            <Switch>
              <Route exact={true} path="/" component={Home} />
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/search" component={List} />
              <Route path="/cart" component={Cart} />
              <Route path="/checkout" component={CheckOut} />
              <Route path="/package" component={Package} />
              <Route path="/order-summary" component={OrderSummery} />
              <Route path="/upload-prescription" component={Prescription} />
              <Route path="/contactUs" component={ContactUs} />
              <Route path="/feedback" component={Feedback} />
              <Route path="/login" component={Login} />
              <Route path="/tasks/:id" component={Task} />
              <Route path="/tasks" component={Tasks} />
              <Route path="/terms" component={TermsAndConditionsScene} />
              <Route path="/privacy" component={PrivacyScene} />
              <Route path="/*" component={NotFound} />
            </Switch>
          </div>
        </Router>
      </App>
    );
  }
}

Main.defaultProps = {
  nav: {
    active: true, // start with nav active
    enabled: true, // start with nav disabled
    responsive: 'multiple',
  },
};

Main.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.shape({
    active: PropTypes.bool,
    enabled: PropTypes.bool,
    responsive: PropTypes.string,
  }),
};

const select = state => ({
  nav: state.nav,
});

export default connect(select)(Main);
