import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Search from 'grommet/components/Search';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import LoginForm from 'grommet/components/LoginForm';

import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Meter from 'grommet/components/Meter';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';
import Menu from 'grommet/components/Menu';
import Actions from 'grommet/components/icons/base/Actions';
import MenuIcon from 'grommet/components/icons/base/Menu';
import Cart from 'grommet/components/icons/base/Cart';
import MailIcon from 'grommet/components/icons/base/Mail';
import User from 'grommet/components/icons/base/User';

import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl';
import Layer from 'grommet/components/Layer';
import Button from 'grommet/components/Button';
import NavControl from '../components/NavControl';

import Responsive from 'grommet/utils/Responsive';
import { loadDashboard, unloadDashboard } from '../actions/dashboard';

import { pageLoaded } from '../screens/utils';

class NavSreach extends Component {
  constructor() {
    super();
    this.state = {
      menu: false,
      small: false,
      login: false,
    };
    this._onResponsive = this._onResponsive.bind(this);
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);

    this._onLogin = this._onLogin.bind(this);
    this._onLoginClose = this._onLoginClose.bind(this);
  }
  componentDidMount() {
    this._responsive = Responsive.start(this._onResponsive);
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive(small) {
    this.setState({ small });
  }
  _onClick() {
    this.setState({ menu: true });
  }
  _onLayerClose() {
    this.setState({ menu: false });
  }
  _onLogin() {
    this.setState({ login: true });
  }
  _onLoginClose() {
    this.setState({ login: false });
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;
    const headers = this.state.small ? (
      <Header fixed={false} colorIndex={'neutral-1-a'} float={false}>
        <Box pad={'small'}>
          <Image alt="logo" size="small" src="../img/logo.svg" />
        </Box>
        <Box flex={true} justify="end" direction="row" responsive={false} />
        <Button
          icon={<MailIcon />}
          plain={true}
          //onClick={...}
          href="#"
        />
        <Button
          icon={<AccessVolumeControl />}
          //onClick={...}

          plain={true}
          href="#"
        />
        <Button icon={<User />} onClick={this._onLogin} plain={true} href="#" />
        <Button icon={<MenuIcon />} onClick={this._onClick} href="#" />
      </Header>
    ) : (
      <Header fixed={false} colorIndex={'neutral-1-a'} float={false}>
        <Box pad={'small'}>
          <Image alt="logo" size="small" src="../img/logo.svg" />
        </Box>

        <Box flex={true} justify="end" direction="row" responsive={false}>
          <Search
            inline={true}
            fill={true}
            size="medium"
            placeHolder="Search"
            dropAlign={{ right: 'right' }}
          />
          <Button
            icon={<Cart />}
            onClick={this._onLogin}
            //label="login"
            plain={true}
            href="#"
          />
          <Button icon={<MenuIcon />} onClick={this._onClick} href="#" />
        </Box>
      </Header>
    );
    const menuLayer = this.state.menu ? (
      <div className="share-layer">
        <Layer
          onClose={this._onLayerClose}
          closer={true}
          overlayClose={true}
          flush={true}
          align={'right'}
          colorIndex="grey-2"
        >
          .
          <Box responsive={true} basis={'large'}>
            <List selectable={true}>
              <ListItem justify="between" separator="horizontal">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Dashboard
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  My Booking
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  My Report
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Cart
                </Heading>
              </ListItem>

              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Feedback/Complain
                </Heading>
              </ListItem>

              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Refer and Earn
                </Heading>
              </ListItem>

              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Sign out
                </Heading>
              </ListItem>
            </List>
          </Box>
        </Layer>
      </div>
    ) : (
      undefined
    );
    const loginLayer = this.state.login ? (
      <div className="share-layer">
        <Layer
          onClose={this._onLoginClose}
          closer={true}
          overlayClose={true}
          flush={true}
          align={'right'}
          colorIndex="grey-2"
        >
          .
          <Box responsive={true} basis={'large'}>
            <LoginForm
              align="start"
              //logo={<Logo className='logo' colorIndex='brand' />}
              title="Synhealth"
              onSubmit={this._onSubmit}
              errors={[error]}
              usernameType="text"
            />
          </Box>
        </Layer>
      </div>
    ) : (
      undefined
    );

    return (
      <Article primary={true}>
        {menuLayer}
        {headers}
        {loginLayer}
      </Article>
    );
  }
}

export default NavSreach;
