import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { headers, buildQuery, processStatus } from 'grommet/utils/Rest';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Search from 'grommet/components/Search';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import LoginForm from 'grommet/components/LoginForm';
import Form from 'grommet/components/Form';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import { login, loginToken, otpLogin, logOut } from '../actions/session';
import { navEnable } from '../actions/nav';

import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Meter from 'grommet/components/Meter';
import Spinning from 'grommet/components/icons/Spinning';
import LoginIcon from 'grommet/components/icons/base/Login';

import { getMessage } from 'grommet/utils/Intl';
import Menu from 'grommet/components/Menu';
import Actions from 'grommet/components/icons/base/Actions';
import MenuIcon from 'grommet/components/icons/base/Menu';
import MailIcon from 'grommet/components/icons/base/Mail';
import User from 'grommet/components/icons/base/User';

import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl';
import Layer from 'grommet/components/Layer';
import Button from 'grommet/components/Button';
import NavControl from '../components/NavControl';

import Responsive from 'grommet/utils/Responsive';

import { pageLoaded } from '../screens/utils';

class NavMenu extends Component {
  constructor() {
    super();
    this.state = {
      menu: false,
      small: false,
      login: false,
      number: '',
      otpShow: true,
      otp: '',
    };
    this._onResponsive = this._onResponsive.bind(this);
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);

    this._onLogin = this._onLogin.bind(this);
    this._onLoginClose = this._onLoginClose.bind(this);
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
    this.otp = this.otp.bind(this);
    this.logOut = this.logOut.bind(this);
  }
  componentDidMount() {
    this._responsive = Responsive.start(this._onResponsive);
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive(small) {
    this.setState({ small });
  }
  _onClick() {
    this.setState({ menu: true });
  }
  _onLayerClose() {
    this.setState({ menu: false });
  }
  _onLogin() {
    this.setState({ login: true });
  }
  _onLoginClose() {
    this.setState({ login: false });
  }
  login() {
    const { dispatch } = this.props;

    const { number } = this.state;
    console.log('helloooo', number);
    dispatch(
      loginToken(this.state.number, () => this.setState({ otpShow: false }))
    );
  }
  otp() {
    const { dispatch } = this.props;
    dispatch(
      otpLogin(this.state.number, this.state.otp, response => {
        this.setState({ login: true });
        this._onLoginClose();
      })
    );
  }
  logOut() {
    const { dispatch } = this.props;
    dispatch(
      logOut(this.state.number, this.state.otp, response => {
        this._onLoginClose();
      })
    );
    this._onLayerClose();
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    const headers = this.state.small ? (
      <Header fixed={false} colorIndex={'neutral-1-a'} float={false}>
        <Box pad={'small'}>
          <Image alt="logo" size="small" src="../img/logo.svg" />
        </Box>
        <Box flex={true} justify="end" direction="row" responsive={false} />
        <Button
          icon={<MailIcon />}
          plain={true}
          //onClick={...}
          href="#"
        />
        <Button
          icon={<AccessVolumeControl />}
          //onClick={...}

          plain={true}
          href="#"
        />
        <Button icon={<User />} onClick={this._onLogin} plain={true} href="#" />
        <Button icon={<MenuIcon />} onClick={this._onClick} href="#" />
      </Header>
    ) : (
      <Header fixed={false} colorIndex={'neutral-1-a'} float={false}>
        <Link to={'/'}>
          <Box pad={'small'}>
            <Image alt="logo" size="small" src="../img/logo.svg" />
          </Box>
        </Link>

        <Box flex={true} justify="end" direction="row" responsive={false} />
        <Button
          icon={<MailIcon />}
          label="support@synhealth.com"
          plain={true}
          //onClick={...}
          href="#"
        />
        <Button
          icon={<AccessVolumeControl />}
          //onClick={...}
          label="8949964630"
          plain={true}
          href="#"
        />
        {this.props.session.token ? (
          <Button
            icon={<User />}
            // onClick={this._onLogin}
            label="User"
            plain={true}
            href="/dashboard"
          />
        ) : (
          <Button
            icon={<User />}
            onClick={this._onLogin}
            label="Login"
            plain={true}
            href="#"
          />
        )}
        <Button icon={<MenuIcon />} onClick={this._onClick} href="#" />
      </Header>
    );
    const menuLayer = this.state.menu ? (
      <div className="share-layer">
        <Layer
          onClose={this._onLayerClose}
          closer={true}
          overlayClose={true}
          flush={true}
          align={'right'}
          colorIndex="grey-2"
        >
          .
          <Box responsive={true} basis={'large'}>
            <List selectable={true}>
              <ListItem justify="between" separator="horizontal">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Dashboard
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  My Booking
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  My Report
                </Heading>
              </ListItem>
              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Cart
                </Heading>
              </ListItem>

              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Feedback/Complain
                </Heading>
              </ListItem>

              <ListItem justify="between">
                <Heading
                  strong={true}
                  uppercase={false}
                  align="center"
                  tag="h4"
                >
                  Refer and Earn
                </Heading>
              </ListItem>

              {this.props.session.token ? (
                <ListItem onClick={this.logOut} justify="between">
                  <Heading
                    strong={true}
                    uppercase={false}
                    align="center"
                    tag="h4"
                  >
                    Sign out
                  </Heading>
                </ListItem>
              ) : (
                ''
              )}
            </List>
          </Box>
        </Layer>
      </div>
    ) : (
      undefined
    );
    const loginLayer = this.state.login ? (
      <div className="share-layer">
        <Layer
          onClose={this._onLoginClose}
          closer={true}
          overlayClose={true}
          align={'right'}
          colorIndex="grey-2"
        >
          .
          <Box responsive={true} pad={'medium'}>
            {this.state.otpShow ? (
              <Form>
                <FormField label="Mobile No.">
                  <TextInput
                    value={this.state.number}
                    id="number"
                    name="number"
                    onDOMChange={e => {
                      this.onChange(e);
                    }}
                  />
                </FormField>
                <Box pad={{ vertical: 'medium' }}>
                  <Button
                    icon={<LoginIcon />}
                    label="Login"
                    onClick={this.login}
                    href="#"
                  />
                </Box>
              </Form>
            ) : (
              <Form>
                <FormField label="ENTER OTP">
                  <TextInput
                    value={this.state.otp}
                    id="otp"
                    name="otp"
                    onDOMChange={e => {
                      this.onChange(e);
                    }}
                  />
                </FormField>
                <Box pad={{ vertical: 'medium' }}>
                  <Button
                    icon={<LoginIcon />}
                    label="Submit OTP"
                    onClick={this.otp}
                    href="#"
                  />
                </Box>
              </Form>
            )}
          </Box>
        </Layer>
      </div>
    ) : (
      undefined
    );

    return (
      <Article primary={true}>
        {menuLayer}
        {headers}
        {loginLayer}
      </Article>
    );
  }
}

NavMenu.defaultProps = {
  session: {
    error: undefined,
  },
};

NavMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  session: PropTypes.shape({
    error: PropTypes.string,
  }),
};

NavMenu.contextTypes = {
  router: PropTypes.object.isRequired,
};

const select = state => ({
  session: state.session,
});

export default connect(select)(NavMenu);
