import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Add from 'grommet/components/icons/base/Add';
import Trash from 'grommet/components/icons/base/Trash';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Button from 'grommet/components/Button';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';

class OrderOverView extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box size={'xxlarge'}  colorIndex="light-2" >
        <Box pad={{ vertical: 'medium' }}>
          <List >
            <ListItem justify="between" separator="none">
              <Heading strong={true} truncate={true} tag="h2">
                Order Overview
              </Heading>
              <Box className="secondary">
                <Button
                  icon={<Add />}
                  label="ADD MORE TESTS"
                  onClick={this._onClick}
                  href="#"
                  plain
                />
              </Box>
            </ListItem>
          </List>
        </Box>

        <List  selectable={true} pad={{ horizontal: 'xlarge' }}>
          <ListItem justify="between" separator="horizontal">
            <Box direction={'column'} justify="start" align="start">
              <Heading strong={true} truncate={true} tag="h3">
                Erythrocyte sedimention Rate{' '}
              </Heading>
              <Button
                plain={true}
                icon={<Trash />}
                label="ADD MORE TESTS"
                onClick={this._onClick}
                href="#"
              />
            </Box>
            <Box
              className="secondary"
              direction={'column'}
              justify="start"
              align="start"
            >
              <span className="secondary">100</span>
              <span className="secondary">80</span>
            </Box>
          </ListItem>
        </List>
      </Box>
    );
  }
}

export default OrderOverView;
