import { SEARCH_LOAD, SEARCH_UNLOAD } from '../actions';
import { createReducer } from './utils';

const initialState = {
  search: []
};

const handlers = {
  [SEARCH_LOAD]: (state, action) => { 
    console.log('actionactionactionaction', action)
    return {
    ...state,
    search: action.payload
  }
}
};

export default createReducer(initialState, handlers);
