import { FAQS_LOAD, FAQS_UNLOAD, FAQ_LOAD, FAQ_UNLOAD } from '../actions';
import { createReducer } from './utils';

const initialState = {
  faqs: [],
  faq: undefined
};

const handlers = {
  [FAQS_LOAD]: (state, action) => { return {
    ...state,
    faqs: action.payload
  }
}
  
  //{packages: action.payload}
  // {
  //   console.log('actionactionactionactionaction', action.payload)
  //   if (!action.error) {
  //     action.payload.error = undefined;
  //     return action.payload;
  //   }
  //   return { error: action.payload };
  // },
  // [POPULARS_UNLOAD]: () => initialState,
  // [POPULAR_LOAD]: (state, action) => {
  //   if (!action.error) {
  //     action.payload.error = undefined;
  //     return action.payload;
  //   }
  //   return { error: action.payload };
  // },
  // [POPULAR_UNLOAD]: () => initialState
};

export default createReducer(initialState, handlers);
